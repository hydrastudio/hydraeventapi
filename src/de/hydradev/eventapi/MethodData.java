package de.hydradev.eventapi;

import java.lang.reflect.Method;

public class MethodData {

    private final Object source;

    private final Method target;

    public MethodData(Object source, Method target) {
        this.source = source;
        this.target = target;
    }

    public Object getSource() {
        return source;
    }

    public Method getTarget() {
        return target;
    }
}
