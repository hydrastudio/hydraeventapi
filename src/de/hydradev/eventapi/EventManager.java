package de.hydradev.eventapi;

import de.hydradev.eventapi.event.Event;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class EventManager {

    private static final HashMap<Class<? extends Event>, List<MethodData>> REGISTRY_MAP = new HashMap<Class<? extends Event>, List<MethodData>>();

    private static boolean hooked = false;

    private static void hook() {
        if(!hooked) {
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                System.out.println(" ");
                System.out.println("------- Copyright HydraStudios (c) 2019 -------");
                System.out.println("HydraEventAPI v" + HydraEventAPI.VERSION + " by " + HydraEventAPI.AUTHOR);
                System.out.println("------- Copyright HydraStudios (c) 2019 -------");
            }));
            hooked = true;
        }
    }

    public static void register(Object object) {
        try {
            for(Method method : object.getClass().getDeclaredMethods()) {
                if(isMethodBad(method))
                    continue;
                register(method, object);
            }
        } catch (Exception e) {
        }
    }

    private static void register(Method method, Object object) {
        Class<? extends  Event> eventClass = (Class<? extends Event>)method.getParameterTypes()[0];

        final MethodData data = new MethodData(object, method);

        if(!data.getTarget().isAccessible())
            data.getTarget().setAccessible(true);

        if(REGISTRY_MAP.containsKey(eventClass)) {
            if(!REGISTRY_MAP.get(eventClass).contains(data)) {
                REGISTRY_MAP.get(eventClass).add(data);
            }
        } else {
            REGISTRY_MAP.put(eventClass, new CopyOnWriteArrayList<MethodData>() {
                {
                    add(data);
                }
            });
        }

    }

    public static Event call(Event e) {
        hook();
        List<MethodData> dataList = REGISTRY_MAP.get(e.getClass());

        if (dataList != null) {
            for(MethodData data : dataList) {
                invoke(data, e);
            }
        }
        return e;
    }

    private static boolean isMethodBad(Method method) {
        return method.getParameterTypes().length != 1 || !method.isAnnotationPresent(EventHandler.class);
    }

    private static void invoke(MethodData data, Event argument) {
        try {
            data.getTarget().invoke(data.getSource(), argument);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
